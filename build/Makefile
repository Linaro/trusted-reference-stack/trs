-include conf.mk

################################################################################
# Paths
################################################################################
ROOT                            ?= $(PWD)
TRS_DOCUMENTATION               ?= $(ROOT)/trs/docs
SCRIPTS                         ?= $(ROOT)/trs/scripts
META_TS                         ?= $(ROOT)/meta-ts
META_TRS                        ?= $(ROOT)/meta-trs
PYTHON_VENV                     ?= $(ROOT)/.pyvenv
TRS_IMAGE                       ?= trs-image

################################################################################
# Flags
################################################################################
TARGET                          ?= all
TRS-TYPE                        ?= baremetal

################################################################################
# Tools
################################################################################
# GNU Make might not execute bash even if it's default system shell...
SHELL := /bin/bash

################################################################################
# Environment variables
################################################################################
# Yocto build variables
DL_DIR          ?= $(HOME)/yocto_cache/downloads
SSTATE_DIR      ?= $(HOME)/yocto_cache/sstate-cache
POKY_ENV        ?= $(ROOT)/poky/oe-init-build-env
# Set a default value. It can be overriden in conf.mk if needed.
NPROC           ?= 16

$(shell mkdir -p $(DL_DIR) $(SSTATE_DIR))

LOCAL_CONF = conf/local.conf

define update_local_conf =
sed -i -e "s|^#DL_DIR.*|DL_DIR = \"$(DL_DIR)\"|" $(LOCAL_CONF) && \
sed -i -e "s|^#SSTATE_DIR.*|SSTATE_DIR = \"$(SSTATE_DIR)\"|" $(LOCAL_CONF) && \
sed -i -e "s|^BB_NUMBER_THREADS.*|BB_NUMBER_THREADS = \"$(NPROC)\"|" $(LOCAL_CONF) && \
sed -i -e "s|^PARALLEL_MAKE.*|PARALLEL_MAKE = \"-j$(NPROC)\"|" $(LOCAL_CONF)
endef

################################################################################
# Sanity checks
# ##############################################################################
# This project and Makefile is based around running it from the root folder. So
# to avoid people making mistakes running it from the "build" folder itself add
# a sanity check that we're indeed are running it from the root.
ifeq ($(wildcard ./.repo), )
$(error 'make' should be run from the root of the project!)
endif

# Both meta-ts and meta-trs are long runnings builds making use 100% use of the
# CPU on all cores on the individual builds. This will make them build
# sequential, which will make the system still fully utilized, but we won't get
# build logs interleaved.
.NOTPARALLEL:

.PHONY: all
all: firmware os
.PHONY: firmware
firmware: meta-ts
.PHONY: os
os: trs

################################################################################
# Software prerequisites
################################################################################
prereqs: apt-prereqs python-prereqs

apt-prereqs:
	@cd $(SCRIPTS) && \
		./install.sh -a

python-prereqs:
	@cd $(SCRIPTS) && \
		PYVENV_PATH=$(PYTHON_VENV) ./install.sh -p -v
	@echo "Before trying to build TRS, don't forget to run:"
	@echo "  source $(PYTHON_VENV)/bin/activate"

################################################################################
# Trusted Substrate
################################################################################

TS_SUPPORTED_TARGETS ?= \
	rockpi4b \
	rpi4 \
	synquacer \
	tsqemuarm64-secureboot \
	imx8mp-verdin \
	zynqmp-kria-starter \
	zynqmp-kria-starter-psa \
	zynqmp-zcu102

ifeq ($(TARGET), all)
TS_BUILD_MSG := "configuring and building meta-ts ts-firmware for all targets"
TS_TARGETS := $(foreach t,$(TS_SUPPORTED_TARGETS),mc:$(t):ts-firmware)
TS_BUILD_FINISHED_MSG := "Build succeeded, see output in build/tmp*/deploy directories."
else
TS_BUILD_MSG := "configuring and building meta-ts ts-firmware for $(TARGET)"
TS_TARGETS := mc:$(TARGET):ts-firmware
TS_BUILD_FINISHED_MSG := "Build succeeded, see output in build/tmp_$(TARGET)/deploy directories."
endif

.PHONY: meta-ts
meta-ts:
	@echo $(TARGET)
	@echo $(TS_BUILD_MSG)
	( rm -rf build/conf && \
	  TEMPLATECONF=$(META_TS)/meta-trustedsubstrate/conf/templates/default . $(POKY_ENV) && \
	  $(update_local_conf) && \
	  cp -a $(META_TS)/meta-trustedsubstrate/conf/templates/multiconfig conf/ && \
	  bitbake $(TS_TARGETS) \
	)
	@echo $(TS_BUILD_FINISHED_MSG)

################################################################################
# TRS
################################################################################
.PHONY: trs

trsconfig:
	@echo "configuring and building TRS"
	( rm -rf build/conf && \
	  TEMPLATECONF=$(META_TRS)/conf/templates/default . $(POKY_ENV) && \
	  $(update_local_conf) && \
	  cp -a $(META_TS)/meta-trustedsubstrate/conf/templates/multiconfig conf/ && \
	  cp -a $(META_TRS)/conf/templates/multiconfig conf/ )

.PHONY: trs
trs: trsconfig
	@echo "configuring and building TRS with development tools (perf, systemtap, etc)"
	( TEMPLATECONF=$(META_TRS)/conf/templates/default . $(POKY_ENV) && \
	  bitbake mc:trs-qemuarm64:$(TRS_IMAGE) )
	@echo "Build succeeded, see output in build/tmp_trs-qemuarm64/deploy directories."

.PHONY: trs32
trs32: trsconfig
	( TEMPLATECONF=$(META_TRS)/conf/templates/default . $(POKY_ENV) && \
	  MACHINE=trs-qemuarm bitbake mc:trs-qemuarm:$(TRS_IMAGE) )
	@echo "Build succeeded, see output in build/tmp_trs-qemuarm32/deploy directories."

trsconfig-sdk: trsconfig
	( TEMPLATECONF=$(META_TRS)/conf/templates/default . $(POKY_ENV) && \
	  echo -e '\nDISTRO_FEATURES += " trs-sdk "' >> $(LOCAL_CONF) )

.PHONY: trs-dev
trs-dev: trsconfig-sdk
	@echo "configuring and building TRS with development tools (perf, systemtap, etc)"
	( TEMPLATECONF=$(META_TRS)/conf/templates/default . $(POKY_ENV) && \
	  echo -e '\nDISTRO_FEATURES += " trs-sdk "' >> $(LOCAL_CONF) && \
	  bitbake mc:trs-qemuarm64:$(TRS_IMAGE) )
	@echo "Build succeeded, see output in build/tmp_trs-qemuarm64/deploy directories."

.PHONY: trs-dev32
trs-dev32: trsconfig-sdk
	( TEMPLATECONF=$(META_TRS)/conf/templates/default . $(POKY_ENV) && \
	  MACHINE=trs-qemuarm bitbake mc:trs-qemuarm:$(TRS_IMAGE) )
	@echo "Build succeeded, see output in build/tmp_trs-qemuarm32/deploy directories."

TRS_ROOTFS_IMG = build/tmp_trs-qemuarm64/deploy/images/trs-qemuarm64/$(TRS_IMAGE)-trs-qemuarm64.rootfs.wic.bz2

ifneq ($(findstring nvme,$(DEV)),)
PART_SUFFIX := p2
else
PART_SUFFIX := 2
endif

.PHONY: flash-trs-rootfs
flash-trs-rootfs:
	@if [ ! "$(DEV)" ]; then \
		echo 'Usage: make flash-trs-rootfs DEV=<device path> (such as /dev/sdb)'; \
		false; \
	fi
	sudo sh -c 'bzcat $(TRS_ROOTFS_IMG) >"$(DEV)"'
	sudo sync "$(DEV)"
	sudo umount "$(DEV)$(PART_SUFFIX)" || :
	sudo growpart $(DEV) 2
	sudo e2fsck -f "$(DEV)$(PART_SUFFIX)"
	sudo resize2fs "$(DEV)$(PART_SUFFIX)"
	sudo sync "$(DEV)"


################################################################################
# Run
################################################################################

define resize_image
	OS_IMAGE=$$(stat -Lc %s $(1)); \
	if [ $$OS_IMAGE -lt $$(expr 10 \* 1024 \* 1024 \* 1024) ]; then \
		echo "File size is less than 10GB. Resizing $(1)..."; \
		qemu-img resize $(1) +10G; \
	fi;
endef

.PHONY: run
run: trsconfig
	@echo "Running meta-ts firmware and meta-trs initramfs, kernel and rootfs"
	( TEMPLATECONF=$(META_TRS)/conf/templates/default . $(POKY_ENV) && \
	  cd tmp_trs-qemuarm64 && \
		set -x && \
		export PATH="`pwd`/`ls -d work/trs_qemuarm64-trs-linux/$(TRS_IMAGE)/*/recipe-sysroot-native/usr/bin | tail -1`:$${PATH}" && \
		$(call resize_image,deploy/images/trs-qemuarm64/$(TRS_IMAGE)-trs-qemuarm64.rootfs.wic) \
		runqemu nographic novga slirp \
	)

.PHONY: run32
run32: trsconfig
	@echo "Running meta-ts firmware and meta-trs initramfs, kernel and rootfs"
	( TEMPLATECONF=$(META_TRS)/conf/templates/default . $(POKY_ENV) && \
	  cd tmp_trs-qemuarm && \
		set -x && \
		export PATH="`pwd`/`ls -d work/trs_qemuarm-trs-linux-gnueabi/$(TRS_IMAGE)/*/recipe-sysroot-native/usr/bin | tail -1`:$${PATH}" && \
		$(call resize_image,deploy/images/trs-qemuarm/$(TRS_IMAGE)-trs-qemuarm.rootfs.wic) && \
		MACHINE=trs-qemuarm runqemu nographic novga slirp \
	)

################################################################################
# Test
################################################################################
.PHONY: test
test: trsconfig
	@echo "Testing meta-ts firmware and meta-trs initramfs, kernel and rootfs"
	( TEMPLATECONF=$(META_TRS)/conf/templates/default . $(POKY_ENV) && \
	  bitbake \
	    -c testimage $(TRS_IMAGE) \
	)

.PHONY: test32
test32: trsconfig
	@echo "Testing meta-ts firmware and meta-trs initramfs, kernel and rootfs"
	( TEMPLATECONF=$(META_TRS)/conf/templates/default . $(POKY_ENV) && \
	  MACHINE=trs-qemuarm bitbake \
	    -c testimage $(TRS_IMAGE) \
	)

################################################################################
# Documentation
################################################################################
docs:
	@echo "Building documentation"
	@cd $(TRS_DOCUMENTATION) && \
		make html

preview-docs: docs
	@echo "Preview documentation"
	@cd $(TRS_DOCUMENTATION) && \
		xdg-open $(TRS_DOCUMENTATION)/_build/html/index.html

################################################################################
# Generate tarballs in DL_DIR when it fetches from revision control
#
# Warning: Use this target only to populate the global downloads mirror,
# otherwise it will increase the build time and consume more storage space
################################################################################
.PHONY: gen-mirror-tar
gen-mirror-tar:
	grep -qxF 'BB_GENERATE_MIRROR_TARBALLS = "1"' build/conf/local.conf \
		|| echo -e '\nBB_GENERATE_MIRROR_TARBALLS = "1"' >> build/conf/local.conf

################################################################################
# Cleaning rules
################################################################################
.PHONY: clean
clean:
	@echo "Cleaning build/tmp, build/conf, build/buildhistory but leaving"
	@echo "rest of the files and caches in place."
	set -x && cd build && \
		DEL="$$( mktemp -d deleted_files.XXXXXXX )" && \
		mv conf tmp* "$$DEL" ; rm -rf "$$DEL" &
	[ -d "build/buildhistory/.git" ] && ( \
		cd build/buildhistory && \
		git rm --quiet -rf . || true )

.PHONY: dist-clean
dist-clean:
	@echo "Cleaning build completely, including all caches"
	rm -rf build

################################################################################
# Find package recipes and dependencies
################################################################################
machine?=trs-qemuarm64
name?=''
.PHONY: find
find:
ifeq ($(name), '')
	$(error You need to provice a recipe name. make find name=<name> machine=<machine>(optional))
endif
	( rm -rf build/conf && \
	  TEMPLATECONF=$(META_TRS)/conf/templates/default . $(POKY_ENV) && \
	  cp -a $(META_TS)/meta-trustedsubstrate/conf/templates/multiconfig conf/ && \
	  cp -a $(META_TRS)/conf/templates/multiconfig conf/ && \
	  bitbake -g mc:$(machine):$(name) && \
	  echo "#############" && \
	  echo "Dependencies:" && \
	  echo "#############" && \
	  grep label task-depends.dot  | grep do_fetch | sed "s/.*\(\.\.\)\/\(.*\)\"]/\2/" && \
	  echo "#############" && \
	  echo "Recipes used:" && \
	  echo "#############" && \
	  bitbake -e mc:$(machine):$(name) | grep BBINCLUDED= | tr " " "\n" | grep -w 'bb\|bbappend' \
	 )

################################################################################
# sanity check for the makefile itself
################################################################################
FAKE_POKY=$(CURDIR)/trs/build/test
mktest:
	POKY_ENV=$(FAKE_POKY)/init-env $(MAKE)

################################################################################
# Local config file
################################################################################
conf.mk:
	touch $@

################################################################################
# Catch all that allows to build individual recipes
################################################################################
%::
	( rm -rf build/conf && \
	  TEMPLATECONF=$(META_TRS)/conf/templates/default . $(POKY_ENV) && \
	  cp -a $(META_TS)/meta-trustedsubstrate/conf/templates/multiconfig conf/ && \
	  cp -a $(META_TRS)/conf/templates/multiconfig conf/ && \
	  bitbake $@ \
	)

