#
# SPDX-License-Identifier: MIT
#

import os

from oeqa.runtime.case import OERuntimeTestCase
from oeqa.runtime.decorator.package import OEHasPackage
from oeqa.core.decorator.oetimeout import OETimeout

class SoafeeTestSuite(OERuntimeTestCase):
    """
    Run SOAFEE Test Suite
    """
    @OETimeout(400)
    @OEHasPackage(['soafee-test-suite'])
    def test_soafee(self):
        cmd = "su -c 'soafee-test-suite run -r' trs ; rm -rf /tmp/soafee-test-suite"
        cmd = "sudo -i -u trs soafee-test-suite run -r -t -T ; rm -rf /tmp/soafee-test-suite"
        status, output = self.target.run(cmd, 300)
        self.assertEqual(status, 0, msg='\n'.join([cmd, output]))
