# Copyright (c) 2021-2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI += "file://20-quiet-printk.conf"

do_install:append() {
    install -Dm 0640 "${UNPACKDIR}/20-quiet-printk.conf" "${D}${sysconfdir}/sysctl.d/20-quiet-printk.conf"
}
